# 3u4osu
3u keyboard for use in [osu!](https://osu.ppy.sh/home).


Models are built for 3d printing and fit
[Teensy 3.2](https://www.pjrc.com/store/teensy32.html).
Supports [Kailh CHOC hot swappable sockets](https://www.adafruit.com/product/5118)
although I had to (hot) glue them onto the top plate.

![Product Image](./product.jpg)

Teensyduino
